#include <time.h>
#include <inttypes.h>
#include <stdio.h>

static struct timespec t_start, t_end;
static bool bAtExitExecuted = false;

static void prof_begin(void)
{
	clock_gettime(CLOCK_REALTIME, &t_start);
}

static void prof_end(void)
{
	if(!bAtExitExecuted)
	{
		clock_gettime(CLOCK_REALTIME, &t_end);

		uint64_t ns_time = ((((uint64_t)(t_end.tv_sec - t_start.tv_sec)) * 1000000000uLL) + 
		                     ((uint64_t)t_end.tv_nsec)) - ((uint64_t) t_start.tv_nsec);

		fprintf(stderr, "wall_clock: %luns\n", ns_time);
		fprintf(stderr, "wall_clock: %lfms\n", ns_time/(1000. * 1000));
		fprintf(stderr, "wall_clock: %lfs\n", ns_time/(1000. * 1000 * 1000));
		bAtExitExecuted = true;
	}
}



prof_begin();
atexit(prof_end);



prof_end();
